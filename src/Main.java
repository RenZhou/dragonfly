import aeroport.TableauVolsFacade;
import database.ConnectionDB;
import database.DAOPilote;
import equipage.InvariantBroken;
import equipage.PNC;
import equipage.Pilote;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        ConnectionDB.resetDatabase();
        ConnectionDB.initDatabase();
        ConnectionDB.loadDatabase();

        TableauVolsFacade tableauVolsFacade = new TableauVolsFacade("Hello");
    }

}
