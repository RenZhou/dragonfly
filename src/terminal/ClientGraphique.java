package terminal;

import aeroport.TableauVolsFacade;
import equipage.PNC;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Iterator;
import java.util.List;

public class ClientGraphique extends JFrame implements ActionListener{

    private String name;

    private JButton btnAddCoPiloteToVol;
    private JButton btnAddPNCToVol;
    private JButton btnAddPiloteToVol;
    private TableauVolsFacade tableauVolsFacade ;

    private JTable table;


    private JPanel content;
    private JPanel pnlCreate;
    private JPanel pnlAttribut;
    private JPanel pnlInputCreate;
    private JPanel pnlInputAtt;
    private JScrollPane scrollPane;

    private JButton btnCreatePilote;
    private JButton btnCreateCoPilote;
    private JButton btnCreatePNC;

    private JTextField txtCreateNom;
    private JTextField txtCreatePrenom;
    private JTextField txtAttNom;
    private JTextField txtAttPrenom;
    private JTextField txtAttVol;

    private void inittable() {
        //Ajouter des enregistrements dans le tableau
        TableModel tableModel = new TableModel();
        for (List<String> row : tableauVolsFacade.getEnregistrement()) {
            tableModel.addRow(row);
        }
        table = new JTable(tableModel);
        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        table.setFillsViewportHeight(true);
        scrollPane = new JScrollPane(table);
    }

    private void initPanel() {
        content = new JPanel(new GridLayout(5,1));

        // Attribut entity to another
        pnlInputAtt = new JPanel();
        txtAttNom = new JTextField("Veuillez entrez ici le nom");
        txtAttNom.setSize(new Dimension(300, 200));
        txtAttPrenom = new JTextField("Veuillez entrez ici le prenom");
        txtAttVol = new JTextField("Ici le ref de vol");
        txtAttPrenom.setSize(new Dimension(300, 200));
        txtAttNom.setMinimumSize(new Dimension(300, 200));
        pnlInputAtt.add(txtAttNom);
        pnlInputAtt.add(txtAttPrenom);
        pnlInputAtt.add(txtAttVol);

        pnlAttribut = new JPanel();
        btnAddPiloteToVol = new JButton("Ajouter Pilote");
        btnAddCoPiloteToVol = new JButton("Ajouter Copilote");
        btnAddPNCToVol = new JButton("Ajouter PNC");
        btnAddPNCToVol.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tableauVolsFacade.addPNCToVol(txtAttVol.getText(), txtAttNom.getText(), txtAttPrenom.getText());
            }
        });
        pnlAttribut.add(btnAddPiloteToVol);
        pnlAttribut.add(btnAddCoPiloteToVol);
        pnlAttribut.add(btnAddPNCToVol);

        pnlInputCreate = new JPanel();
        txtCreateNom = new JTextField("Veuillez entrez ici le nom");
        txtCreateNom.setSize(new Dimension(300, 200));
        txtCreatePrenom = new JTextField("Veuillez entrez ici le prenom");
        txtCreatePrenom.setSize(new Dimension(300, 200));
        txtCreateNom.setMinimumSize(new Dimension(300, 200));
        pnlInputCreate.add(txtCreateNom);
        pnlInputCreate.add(txtCreatePrenom);

        // Create Entities
        pnlCreate = new JPanel();
        JButton btnShowMyVol = new JButton("Afficher Mes vols");
        btnShowMyVol.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFrame jFrame = new JFrame("Afficher les vols de ");
                jFrame.setSize(700, 500);
                jFrame.setLocationRelativeTo(null);

                TableModel tableModel = new TableModel();
                for (List<String> row : tableauVolsFacade.getEnregistrement(txtCreateNom.getText(), txtCreatePrenom.getText())) {
                    tableModel.addRow(row);
                }
                table = new JTable(tableModel);
                table.setPreferredScrollableViewportSize(new Dimension(500, 70));
                table.setFillsViewportHeight(true);
                scrollPane = new JScrollPane(table);

                jFrame.add(scrollPane);

                jFrame.setVisible(true);
            }
        });

        btnCreatePilote = new JButton("Créer un Pilote");
        btnCreatePilote.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tableauVolsFacade.createPilote(txtCreateNom.getText(), txtCreatePrenom.getText());
            }
        });

        btnCreateCoPilote = new JButton("Créer un Copilote");
        btnCreatePNC = new JButton("Créer un PNC");
        btnCreatePNC.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tableauVolsFacade.createPNC(txtCreateNom.getText(), txtCreatePrenom.getText());
            }
        });
        pnlCreate.add(btnShowMyVol);
        pnlCreate.add(btnCreatePilote);
        pnlCreate.add(btnCreateCoPilote);
        pnlCreate.add(btnCreatePNC);

        content.add(scrollPane);
        content.add(pnlInputAtt);
        content.add(pnlAttribut);
        content.add(pnlInputCreate);
        content.add(pnlCreate);
    }

    private void writeInfo() {
        JSONObject obj = new JSONObject();
        obj.put("name", "drafly-compagnie");
        obj.put("capital", new Integer(55000));

        JSONArray associe = new JSONArray();
        associe.add("Ren ZHOU");
        associe.add("Hello Hi");

        obj.put("associe", associe);

        try {

            FileWriter file = new FileWriter(".\\aeroport.json");
            file.write(obj.toJSONString());
            file.flush();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readInfo() {
        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader(".\\aeroport.json"));

            JSONObject jsonObject = (JSONObject) obj;

            name = (String) jsonObject.get("name");
            System.out.println("Compagnie name : " + name);

            long capital = (Long) jsonObject.get("capital");
            System.out.println("Compagnie capital : " + capital);

            // loop array
            JSONArray msg = (JSONArray) jsonObject.get("associe");
            Iterator<String> iterator = msg.iterator();
            System.out.println("Les associes sont : ");
            while (iterator.hasNext()) {
                System.out.println("\t" + iterator.next());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public ClientGraphique() {
        writeInfo();

        readInfo();

        tableauVolsFacade = new TableauVolsFacade("AVION");
        this.setTitle("Tableau de bord : " + name);
        this.setSize(700, 500);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        inittable();
        initPanel();
        this.add(content);

        this.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == btnCreatePilote) {
            System.out.println(txtCreateNom.getText());
        }
    }

    public static void main(String arg[]) {
        ClientGraphique clientGraphique = new ClientGraphique();
    }
}
