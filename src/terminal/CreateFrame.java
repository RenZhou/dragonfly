package terminal;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ren on 11/01/2016.
 */
public class CreateFrame extends JFrame implements ActionListener {

    private JPanel content;
    private JPanel pnlCreate;

    private JButton btnCreatePilote;
    private JButton btnCreateCoPilote;
    private JButton btnCreatePNC;

    public CreateFrame() {
        this.setTitle("Creation des entités");
        this.setSize(700, 500);
        this.setLocationRelativeTo(null);

        content = new JPanel(new GridLayout(3,1));

        pnlCreate = new JPanel();
        btnCreatePilote = new JButton("Créer un Pilote");
        btnCreateCoPilote = new JButton("Créer un Copilote");
        btnCreatePNC = new JButton("Créer un PNC");
        pnlCreate.add(btnCreatePilote);
        pnlCreate.add(btnCreateCoPilote);
        pnlCreate.add(btnCreatePNC);

        content.add(pnlCreate);
        this.add(content);

        this.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {

    }
}
