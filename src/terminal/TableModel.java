package terminal;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ren on 11/01/2016.
 */
public class TableModel extends AbstractTableModel {
    private List<String> columnNames = new ArrayList();
    private List<List> data = new ArrayList();

    {
        columnNames.add("Avion");
        columnNames.add("Vol");
        columnNames.add("Dest");
        columnNames.add("Date");
        columnNames.add("Type");
        columnNames.add("Site");
        columnNames.add("Prenom");
        columnNames.add("Nom");
        columnNames.add("Fonction");
    }

    public void addRow(List rowData) {
        data.add(rowData);
        fireTableRowsInserted(data.size() - 1, data.size() - 1);
    }

    public int getColumnCount() {
        return columnNames.size();
    }

    public int getRowCount() {
        return data.size();
    }

    public String getColumnName(int col) {
        try {
            return columnNames.get(col);
        } catch (Exception e) {
            return null;
        }
    }

    public Object getValueAt(int row, int col) {
        return data.get(row).get(col);
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

}
