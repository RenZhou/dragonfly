package terminal;

import javax.swing.*;

/**
 * Created by ren on 09/01/2016.
 */
public class TableauDeBord extends JFrame{
    private JPanel content;
    private JPanel tableau;
    private JTable tableauVol;

    TableauDeBord() {
        super("Tableau de bord");
        this.setContentPane(content);

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String arg[]) {
        TableauDeBord tableauDeBord = new TableauDeBord();
    }
}
