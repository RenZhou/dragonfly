package equipage;

public class PNC extends Personne {

    public PNC(String nom, String prenom) {
        super(nom, prenom);
    }

    @Override
    public String toString() {
        return super.toString() + ", Fonction : PNC";
    }
}
