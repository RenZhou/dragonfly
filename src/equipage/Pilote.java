package equipage;

public class Pilote extends Personne {

    public Pilote(String nom, String prenom) {
        super(nom, prenom);
    }

    @Override
    public String toString() {
        return super.toString() + ", Fonction : Pilote";
    }
}
