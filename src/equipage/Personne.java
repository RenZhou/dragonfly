package equipage;

import avion.TypeAvion;

import java.util.ArrayList;
import java.util.List;

public abstract class Personne {
    private String nom;
    private String prenom;
    private List<TypeAvion> avionHandle;

    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
        avionHandle = new ArrayList<TypeAvion>();
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public boolean peutVoler(TypeAvion typeAvion) {
        for (TypeAvion avion : avionHandle) {
            if (avion == typeAvion) {
                return true;
            }
        }
        return false;
    }

    public boolean addQualification(TypeAvion typeAvion) throws EquipageException, InvariantBroken {
        for (TypeAvion t : avionHandle) {
            if (t == typeAvion) {
                throw new InvariantBroken();
            }
        }
        if (avionHandle.size() >= 2) {
            throw new EquipageException();
        }
        avionHandle.add(typeAvion);
        typeAvion.addQualifie(this);
        return true;
    }

    public boolean delQualification(TypeAvion typeAvion, boolean fromType) throws EquipageException {
        if (avionHandle.size() < 0) {
            throw new EquipageException();
        }
        avionHandle.remove(typeAvion);
        if (!fromType) {
            typeAvion.delQualifie(this);
        }
        return true;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Personne)) return false;
        Personne otherMyClass = (Personne) other;
        if (!this.nom.equals(otherMyClass.getNom())) return false;
        if (!this.prenom.equals(otherMyClass.getPrenom())) return false;
        return true;
    }

    @Override
    public String toString() {
        return "Nom : " + nom + ", Prenom :" + prenom;
    }
}
