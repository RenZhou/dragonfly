package equipage;

import aeroport.Vol;

import java.util.ArrayList;
import java.util.List;

public class Equipage {

    private Pilote pilote;
    private CoPilote coPilote;
    private List<PNC> listPNC;
    private List<Vol> listVol;
    private Vol vol;
    private int minPNC;
    private int maxPNC;

    public Equipage() {
        listPNC = new ArrayList<PNC>();
        listVol = new ArrayList<Vol>();
        minPNC = 2;
        maxPNC = 4;
    }

    public Equipage(Vol vol) {
        this();
        listVol.add(vol);
        this.vol = vol;
    }

    public void addPilote(Pilote pilote) throws EquipageException {
        if (this.pilote != null) {
            throw new EquipageException();
        }
        this.pilote = pilote;
    }

    public void addCoPilote(CoPilote coPilote) throws EquipageException {
        if (this.coPilote != null) {
            throw new EquipageException();
        }
        this.coPilote = coPilote;
    }


    public void addPNC(List<PNC> allPNC) throws EquipageException {
        for (PNC p : allPNC) {
            addPNC(p);
        }
    }

    public void addPNC(PNC pnc) throws EquipageException {
        for (PNC p : listPNC) {
            if (p == pnc) {
                throw new EquipageException();
            }
        }
        if (listPNC.size() >= getMaxPNC()) {
            throw new EquipageException();
        }
        listPNC.add(pnc);
    }

    public boolean hasPilote() {
        return pilote != null;
    }

    public boolean hasCoPilote() {
        return coPilote != null;
    }

    public boolean hasSufficientPNC() {
        return listPNC.size() >= getMinPNC() &&
                listPNC.size() <= getMaxPNC();
    }

    public boolean isComplet() {
        return hasCoPilote() && hasPilote() && hasSufficientPNC();
    }

    public int getMinPNC() {
        return minPNC;
    }

    public void setMinPNC(int minPNC) {
        this.minPNC = minPNC;
    }

    public int getMaxPNC() {
        return maxPNC;
    }

    public void setMaxPNC(int maxPNC) {
        this.maxPNC = maxPNC;
    }

    public boolean peutVoler(Vol vol) {
        return false;
    }

    @Override
    public String toString() {
        String str = "";
        str += "Pilote : " + pilote + "\n";
        str += "CoPilote : " + coPilote + "\n";
        str += "PNC : ";
        for (PNC p : listPNC) {
            str += "\t" + p + "\n";
        }
        return str;
    }

    public Pilote getPilote() {
        return pilote;
    }

    public CoPilote getCoPilote() {
        return coPilote;
    }

    public Vol getVol() {
        return vol;
    }

    public List<PNC> getListPNC() {
        return listPNC;
    }

}
