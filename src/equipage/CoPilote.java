package equipage;

public class CoPilote extends Personne {

    public CoPilote(String nom, String prenom) {
        super(nom, prenom);
    }

    @Override
    public String toString() {
        return super.toString() + ", Fonction : CoPilote";
    }
}
