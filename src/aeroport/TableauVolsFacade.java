package aeroport;

import avion.Avion;
import avion.TypeAvion;
import database.*;
import equipage.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Facade
 */
public class TableauVolsFacade implements ITableauVols {

    private String nom;

    private List<Vol> lesVols;
    private List<TypeAvion> lesTypeAvion;
    private List<Avion> lesAvion;
    private List<Pilote> lesPilotes;
    private List<CoPilote> lesCoPilotes;
    private List<PNC> lesPNC;
    private List<Equipage> lesEquipages;

    private DAOPilote daoPilote;
    private DAOCoPilote daoCoPilote;
    private DAOPNC daopnc;
    private DAOTypeAvion daoTypeAvion;
    private DAOAvion daoAvion;
    private DAOEquipage daoEquipage;
    private DAOVol daoVol;

    public TableauVolsFacade(String nom) {
        this.nom = nom;
        lesVols = new ArrayList<Vol>();
        lesTypeAvion = new ArrayList<TypeAvion>();
        lesAvion = new ArrayList<Avion>();
        lesPilotes = new ArrayList<Pilote>();
        lesCoPilotes = new ArrayList<CoPilote>();
        lesPNC = new ArrayList<PNC>();
        lesEquipages = new ArrayList<Equipage>();

        daoVol = new DAOVol();
        daoAvion = new DAOAvion();
        daoTypeAvion = new DAOTypeAvion();
        daoPilote = new DAOPilote();
        daoCoPilote = new DAOCoPilote();
        daopnc = new DAOPNC();
        daoEquipage = new DAOEquipage();

        loadFromDatabase();
    }

    private void loadFromDatabase() {
        try {
            lesTypeAvion.addAll(daoTypeAvion.getAllTypeAvion());
            lesAvion.addAll(daoAvion.getAllAvion(lesTypeAvion));
            lesPilotes.addAll(daoPilote.getAllPilote());
            lesCoPilotes.addAll(daoCoPilote.getAllCoPilote());
            lesPNC.addAll(daopnc.getAllPNC());
            lesVols.addAll(daoVol.getAllVol(lesAvion));
            lesEquipages.addAll(daoEquipage.getAllEquipage(lesVols, lesPilotes, lesCoPilotes, lesPNC));
        } catch (InvariantBroken invariantBroken) {
            invariantBroken.printStackTrace();
        }
    }


    public Pilote createPilote(String nom, String prenom) {
        Pilote pilote = new Pilote(nom, prenom);
        try {
            daoPilote.addPilote(pilote);
        } catch (InvariantBroken invariantBroken) {
            invariantBroken.printStackTrace();
        }
        lesPilotes.add(pilote);
        return pilote;
    }

    public CoPilote createCoPilote(String nom, String prenom) {
        CoPilote coPilote = new CoPilote(nom, prenom);
        try {
            daoCoPilote.addCoPilote(coPilote);
        } catch (InvariantBroken invariantBroken) {
            invariantBroken.printStackTrace();
        }
        lesCoPilotes.add(coPilote);
        return coPilote;
    }

    public PNC createPNC(String nom, String prenom) {
        PNC pnc = new PNC(nom, prenom);
        try {
            daopnc.addPNC(pnc);
        } catch (InvariantBroken invariantBroken) {
            invariantBroken.printStackTrace();
        }
        lesPNC.add(pnc);
        return pnc;
    }

    public TypeAvion createTypeAvion(String nom) {
        TypeAvion typeAvion = new TypeAvion(nom);
        try {
            daoTypeAvion.addTypeAvion(typeAvion);
        } catch (InvariantBroken invariantBroken) {
            invariantBroken.printStackTrace();
        }
        lesTypeAvion.add(typeAvion);
        return typeAvion;
    }

    public Avion createAvion(TypeAvion typeAvion, String ref) {
        Avion avion = new Avion(typeAvion, ref);
        try {
            daoAvion.addAvion(avion);
        } catch (InvariantBroken invariantBroken) {
            invariantBroken.printStackTrace();
        }
        lesAvion.add(avion);
        return avion;
    }

    public void addPiloteToVol(Vol vol, Pilote pilote) {
        try {
            vol.addPilote(pilote);
        } catch (EquipageException e) {
            e.printStackTrace();
        }
    }

    public void addCoPiloteToVol(Vol vol, CoPilote coPilote) {
        try {
            vol.addCoPilote(coPilote);
        } catch (EquipageException e) {
            e.printStackTrace();
        }
    }

    public void addPNCToVol(String ref, String nom, String prenom) {
        Vol vol = null;
        for (Vol v : lesVols) {
            if (v.getRef().equals(ref)) {
                vol = v;
                break;
            }
        }
        PNC pnc = null;
        for (PNC p : lesPNC) {
            if (p.getNom().equals(nom) && p.getPrenom().equals(prenom)) {
                pnc = p;
                break;
            }
        }
        addPNCToVol(vol, pnc);
    }

    public void addPNCToVol(Vol vol, PNC pnc) {
        try {
            vol.addPNC(pnc);
        } catch (EquipageException e) {
            e.printStackTrace();
        }
    }

    public void addAvionToVol(Vol vol, Avion avion) {
        vol.addAvion(avion);
    }

    public void addPersonneToTypeAvion(TypeAvion typeAvion, Personne personne) {
        try {
            typeAvion.addQualifie(personne);
        } catch (EquipageException e) {
            e.printStackTrace();
        }
    }

    public void addMaxPNCToTypeAvion(TypeAvion typeAvion, int max) {
        typeAvion.setMaxPNC(max);
    }

    public void addMinPNCToTypeAvion(TypeAvion typeAvion, int min) {
        typeAvion.setMinPNC(min);
    }

    public void showVols() {
        for (Vol vol : lesVols) {
            System.out.println(vol);
        }
    }

    public List<List<String>> getEnregistrement(String nom, String prenom) {
        List<List<String>> enregistrement = new ArrayList<List<String>>();

        for (Vol vol : lesVols) {
            //Add Pilote
            if (vol.hasPilote()) {
                if (vol.getEquipage().getPilote().getPrenom().equals(prenom) && vol.getEquipage().getPilote().getNom().equals(nom)) {
                    List<String> pilote = new ArrayList<String>();
                    pilote.addAll(vol.getInfo());
                    pilote.add(vol.getEquipage().getPilote().getPrenom());
                    pilote.add(vol.getEquipage().getPilote().getNom());
                    pilote.add("Pilote");
                    enregistrement.add(pilote);
                }
            }
            if (vol.hasCoPilote()) {
                if (vol.getEquipage().getCoPilote().getPrenom().equals(prenom) && vol.getEquipage().getCoPilote().getNom().equals(nom)) {
                    List<String> coPilote = new ArrayList<String>();
                    coPilote.addAll(vol.getInfo());
                    coPilote.add(vol.getEquipage().getCoPilote().getPrenom());
                    coPilote.add(vol.getEquipage().getCoPilote().getNom());
                    coPilote.add("CoPilote");
                    enregistrement.add(coPilote);
                }
            }
            for (PNC p : vol.getEquipage().getListPNC()) {
                if (p.getPrenom().equals(prenom) && p.getNom().equals(nom)) {
                    List<String> pnc = new ArrayList<String>();
                    pnc.addAll(vol.getInfo());
                    pnc.add(p.getPrenom());
                    pnc.add(p.getNom());
                    pnc.add("PNC");
                    enregistrement.add(pnc);
                    break;
                }
            }
        }

        return enregistrement;
    }

    public List<List<String>> getEnregistrement() {
        List<List<String>> enregistrement = new ArrayList<List<String>>();

        for (Vol vol : lesVols) {
            if (vol.equipageAuComplet()) {
                //Add Pilote
                List<String> pilote = new ArrayList<String>();
                pilote.addAll(vol.getInfo());
                pilote.add(vol.getEquipage().getPilote().getPrenom());
                pilote.add(vol.getEquipage().getPilote().getNom());
                pilote.add("Pilote");
                enregistrement.add(pilote);
                List<String> coPilote = new ArrayList<String>();
                coPilote.addAll(vol.getInfo());
                coPilote.add(vol.getEquipage().getCoPilote().getPrenom());
                coPilote.add(vol.getEquipage().getCoPilote().getNom());
                coPilote.add("CoPilote");
                enregistrement.add(coPilote);

                for (PNC p : vol.getEquipage().getListPNC()) {
                    List<String> pnc = new ArrayList<String>();
                    pnc.addAll(vol.getInfo());
                    pnc.add(p.getPrenom());
                    pnc.add(p.getNom());
                    pnc.add("PNC");
                    enregistrement.add(pnc);
                }
            }
        }

        return enregistrement;
    }
}
