package aeroport;

import avion.Avion;
import equipage.*;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Vol {
    private String ref;
    private String orig;
    private String dest;
    private Avion avion;
    private VolDate dateDep;
    private Equipage equipage;

    private class VolDate {
        Date date;

        VolDate(Date date) {
            this.date = date;
        }

        @Override
        public String toString() {
            DateFormat shortDateFormat = DateFormat.getDateTimeInstance(
                    DateFormat.SHORT,
                    DateFormat.SHORT);
            return shortDateFormat.format(date);
        }
    }

    public Equipage getEquipage() {
        return equipage;
    }


    public String getDest() {
        return dest;
    }

    public void addEquipage(Equipage equipage) {
        this.equipage = equipage;
    }

    public Vol(String num, Date dep) {
        this.ref = num;
        this.dateDep = new VolDate(dep);
        equipage = new Equipage(this);
    }

    public Vol(String num, String orig, String dest, Avion avion, Date dep) {
        this(num, dep);
        this.orig = orig;
        this.dest = dest;
        this.avion = avion;
    }

    public void addPilote(Pilote pilote) throws EquipageException {
        equipage.addPilote(pilote);
    }

    public void addCoPilote(CoPilote copilote) throws EquipageException {
        equipage.addCoPilote(copilote);
    }

    public void addPNC(PNC pnc) throws EquipageException {
        equipage.addPNC(pnc);
    }

    public void addAvion(Avion avion) {
        if (this.avion == null) {
            this.avion = avion;
        }
    }

    public boolean hasPilote() {
        return equipage.hasPilote();
    }

    public boolean hasCoPilote() {
        return equipage.hasCoPilote();
    }

    public VolDate getDep() {
        return dateDep;
    }

    public void setDep(Date dep) {
        this.dateDep = new VolDate(dep);
    }

    public Avion getAvion() {
        return avion;
    }

    public int getMinPNC() {
        return avion.getTypeAvion().getMinPNC();
    }

    public int getMaxPNC() {
        return avion.getTypeAvion().getMaxPNC();
    }

    public String getRef() {
        return ref;
    }

    public boolean equipageAuComplet() {
        return equipage.isComplet();
    }

    public boolean peutVoler() {
        return orig != null && dest != null && avion != null && equipageAuComplet();
    }

    @Override
    public String toString() {
        String str = "";
        str += "Avion : " + avion + "\n";
        str += "Ref   : " + ref + "\n";
        str += "Dest  : " + dest + "\n";
        str += "Date  : " + dateDep + "\n";
        str += "type  : " + avion.getTypeAvion() + "\n";
        str += "Site  : " + orig + "\n";
        str += "Equip : " + equipage;
        return str;
    }

    public List<String> getInfo() {
        List<String> enregistrement = new ArrayList<String>();
        enregistrement.add(getAvion().getRef());
        enregistrement.add(getRef());
        enregistrement.add(getDest());
        enregistrement.add(getAvion().getTypeAvion().getNom());
        enregistrement.add(dateDep.toString());
        enregistrement.add(orig);
        return enregistrement;
    }
}
