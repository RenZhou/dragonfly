package aeroport;

import avion.Avion;
import avion.TypeAvion;
import equipage.CoPilote;
import equipage.PNC;
import equipage.Personne;
import equipage.Pilote;

import java.lang.reflect.Type;

/**
 * Created by ren on 10/01/2016.
 */
public interface ITableauVols {

    //Creation des objects
    Pilote createPilote(String nom, String prenom);

    CoPilote createCoPilote(String nom, String prenom);

    PNC createPNC(String nom, String prenom);

    TypeAvion createTypeAvion(String nom);

    Avion createAvion(TypeAvion typeAvion, String ref);

    //Ajout pour les vols
    void addPiloteToVol(Vol vol, Pilote pilote);

    void addCoPiloteToVol(Vol vol, CoPilote coPilote);

    void addPNCToVol(Vol vol, PNC pnc);

    void addAvionToVol(Vol vol, Avion avion);

    //Ajout pour les typeavions
    void addPersonneToTypeAvion(TypeAvion typeAvion, Personne personne);

    void addMaxPNCToTypeAvion(TypeAvion typeAvion, int max);

    void addMinPNCToTypeAvion(TypeAvion typeAvion, int min);
}
