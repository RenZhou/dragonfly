package test;

import aeroport.Vol;
import equipage.*;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class EquipageTest {

    @org.junit.Test
    public void testPeutInitialiser() throws Exception {
        Vol vol = new Vol("hello", new Date());

        Equipage equipage = new Equipage();
        Equipage equipage2 = new Equipage(vol);
    }

    @Test
    public void testAddPilote() throws Exception {
        Equipage equipage = new Equipage();
        Pilote p1 = new Pilote("zhou", "ren");

        assertFalse(equipage.hasPilote());

        equipage.addPilote(p1);
        assertTrue(equipage.hasPilote());
    }

    @Test
    public void testAddCoPilote() throws Exception {
        Equipage equipage = new Equipage();
        CoPilote p1 = new CoPilote("zhou", "ren");

        assertFalse(equipage.hasPilote());

        equipage.addCoPilote(p1);
        assertTrue(equipage.hasCoPilote());
    }

    @Test(expected = EquipageException.class)
    public void testAddPNC() throws Exception {
        Equipage equipage = new Equipage();
        PNC pnc1 = new PNC("zhou", "ren");
        PNC pnc2 = new PNC("zhou", "ren");
        PNC pnc3 = new PNC("zhou", "ren");
        PNC pnc4 = new PNC("zhou", "ren");
        PNC pnc5 = new PNC("zhou", "ren");

        assertFalse(equipage.hasSufficientPNC());
        equipage.addPNC(pnc1);
        equipage.addPNC(pnc2);
        assertTrue(equipage.hasSufficientPNC());
        equipage.addPNC(pnc3);
        equipage.addPNC(pnc4);
        assertTrue(equipage.hasSufficientPNC());
        equipage.addPNC(pnc5);
    }

    @Test
    public void testIsComplet() throws Exception {
        Equipage equipage = new Equipage();
        Pilote p1 = new Pilote("zhou", "ren");
        CoPilote cp1 = new CoPilote("zhou", "ren");
        PNC pnc1 = new PNC("zhou", "ren");
        PNC pnc2 = new PNC("zhou", "ren");
        PNC pnc3 = new PNC("zhou", "ren");

        assertFalse(equipage.isComplet());
        equipage.addCoPilote(cp1);
        equipage.addPilote(p1);
        equipage.addPNC(pnc1);
        equipage.addPNC(pnc2);
        assertTrue(equipage.isComplet());
        equipage.setMinPNC(3);
        assertFalse(equipage.isComplet());

        equipage.addPNC(pnc3);
        assertTrue(equipage.isComplet());

    }
}