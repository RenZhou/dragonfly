package test;

import equipage.PNC;

import static org.junit.Assert.*;

/**
 * Created by ren on 09/01/2016.
 */
public class PNCTest {

    @org.junit.Test
    public void testPeutInitialiser() throws Exception {
        PNC pnc = new PNC("zhou", "ren");
        assertEquals("zhou", pnc.getNom());
        assertEquals("ren", pnc.getPrenom());
    }

}