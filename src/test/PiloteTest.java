package test;

import avion.TypeAvion;
import equipage.EquipageException;
import equipage.Pilote;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ren on 09/01/2016.
 */
public class PiloteTest {

    private Pilote pilote;

    @Before
    public void setUp() throws Exception {
        pilote = new Pilote("zhou", "ren");
    }

    @org.junit.Test
    public void testPeutInitialiser() throws Exception {
        assertEquals("zhou", pilote.getNom());
        assertEquals("ren", pilote.getPrenom());
    }

    @org.junit.Test
    public void testPeutVoler() throws Exception {
        TypeAvion typeAvion = new TypeAvion("BOEING");

        assertFalse(pilote.peutVoler(typeAvion));

        pilote.addQualification(typeAvion);
        assertTrue(pilote.peutVoler(typeAvion));

        pilote.delQualification(typeAvion, true);
        assertFalse(pilote.peutVoler(typeAvion));
    }

    @Test(expected = EquipageException.class)
    public void testOnlyTwoTypeAvion() throws Exception {
        TypeAvion typeAvion = new TypeAvion("BOEING");
        TypeAvion typeAvion1 = new TypeAvion("AIRBUS");
        TypeAvion typeAvion2 = new TypeAvion("JOUET");
        pilote.addQualification(typeAvion);
        pilote.addQualification(typeAvion1);
        pilote.addQualification(typeAvion2);
    }

    @Test(expected = EquipageException.class)
    public void testMustNotRemoveQualifEmpty() throws Exception {
        TypeAvion typeAvion = new TypeAvion("BOEING");
        pilote.delQualification(typeAvion, false);
    }
}