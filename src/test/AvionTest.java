package test;

import avion.Avion;
import avion.TypeAvion;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ren on 10/01/2016.
 */
public class AvionTest {

    private Avion avion;

    @Before
    public void setUp() throws Exception {
        TypeAvion typeAvion = new TypeAvion("Boeing");
        avion = new Avion(typeAvion, "B770");
    }

    @Test
    public void testGetRef() throws Exception {
        assertEquals("B770", avion.getRef());
    }

    @Test
    public void testGetTypeAvion() throws Exception {
        TypeAvion typeAvion = new TypeAvion("Boeing");
        assertEquals(typeAvion, avion.getTypeAvion());
    }
}