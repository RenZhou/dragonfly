package test;

import aeroport.Vol;
import avion.Avion;
import avion.TypeAvion;
import equipage.CoPilote;
import equipage.PNC;
import equipage.Pilote;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by ren on 10/01/2016.
 */
public class VolTest {

    Vol vol;
    Date dateDep;

    @Before
    public void setUp() throws Exception {
        dateDep = new Date();
        vol = new Vol("A3306", dateDep);
    }

    @Test
    public void testAddPilote() throws Exception {
        Pilote pilote = new Pilote("zhou", "ren");
        vol.addPilote(pilote);
    }

    @Test
    public void testAddCoPilote() throws Exception {
        CoPilote coPilote = new CoPilote("zhou", "ren");
        vol.addCoPilote(coPilote);
    }

    @Test
    public void testAddPNC() throws Exception {
        PNC pnc = new PNC("zhou", "ren");
        vol.addPNC(pnc);
    }

    @Test
    public void testEquipageAuComplet() throws Exception {
        Pilote pilote = new Pilote("zhou", "ren");
        CoPilote coPilote = new CoPilote("zhou", "ren");
        PNC pnc = new PNC("zhou", "ren");
        PNC pnc2 = new PNC("zhou", "ren2");

        assertFalse(vol.equipageAuComplet());

        vol.addPilote(pilote);
        vol.addCoPilote(coPilote);
        vol.addPNC(pnc);
        assertFalse(vol.equipageAuComplet());

        vol.addPNC(pnc2);
        assertTrue(vol.equipageAuComplet());
    }

    @Test
    public void testAddAvion() throws Exception {
        TypeAvion typeAvion = new TypeAvion("BOEING");
        Avion avion = new Avion(typeAvion, "5432");
        vol.addAvion(avion);
    }

    @Test
    public void testToString() throws Exception {
        TypeAvion typeAvion = new TypeAvion("BOEING");
        Avion avion = new Avion(typeAvion, "5432");
        Pilote pilote = new Pilote("zhou", "ren");
        CoPilote coPilote = new CoPilote("zhou", "ren");
        PNC pnc = new PNC("zhou", "ren");
        PNC pnc2 = new PNC("zhou", "ren2");

        vol.addPilote(pilote);
        vol.addCoPilote(coPilote);
        vol.addPNC(pnc);
        vol.addPNC(pnc2);
        vol.addAvion(avion);

        System.out.println(vol);
    }
}