package database;

import equipage.InvariantBroken;
import equipage.Personne;

import java.sql.*;

public class DAOPersonne {
    private Connection conn;
    private PreparedStatement preparedStatement;

    public DAOPersonne() {
        conn = ConnectionDB.getConnection();
    }

    public int addPersonne(Personne personne) throws InvariantBroken {
        int idPersonne = 0;
        try {
            String addPersonneQuery = "INSERT INTO PERSONNE(id, nom, prenom) VALUES(NULL,?,?)";
            preparedStatement = conn.prepareStatement(addPersonneQuery, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, personne.getNom());
            preparedStatement.setString(2, personne.getPrenom());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            while (rs.next()) {
                idPersonne = rs.getInt(1);
            }
            System.out.println("Pilote numero " + idPersonne + " a ete cree");
        } catch (Exception e) {
            throw new InvariantBroken();
        }
        return idPersonne;
    }

    public int searchPersonne(Personne personne) throws InvariantBroken {
        try {
            int idPersonne = 0;
            String searchPNCQuery = "SELECT id FROM PERSONNE WHERE nom = ? and prenom = ?";
            preparedStatement = conn.prepareStatement(searchPNCQuery);
            preparedStatement.setString(1, personne.getNom());
            preparedStatement.setString(2, personne.getPrenom());
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                idPersonne = rs.getInt(1);
            }
            return idPersonne;
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public void removePersonne(Personne personne) {
        try {
            String removePersonneQuery = "DELETE FROM PERSONNE WHERE nom = ? and prenom = ?";
            preparedStatement = conn.prepareStatement(removePersonneQuery);
            preparedStatement.setString(1, personne.getNom());
            preparedStatement.setString(2, personne.getPrenom());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
