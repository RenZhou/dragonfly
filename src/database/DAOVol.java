package database;

import aeroport.Vol;
import avion.Avion;
import equipage.InvariantBroken;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DAOVol {

    private Connection conn;
    private PreparedStatement preparedStatement;

    public DAOVol() {
        conn = ConnectionDB.getConnection();
    }

    public void addVol(String ref, String orig, String dest, Date datedep) {

    }

    public int searchVol(Vol vol) throws InvariantBroken {
        try {
            int idVol = 0;
            String searchPNCQuery = "SELECT * FROM VOl WHERE ref = ?";
            preparedStatement = conn.prepareStatement(searchPNCQuery);
            preparedStatement.setString(1, vol.getRef());
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                idVol = rs.getInt(1);
            }
            return idVol;
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }


    public List<Vol> getAllVol(List<Avion> lesAvion) {
        try {
            DAOAvion daoAvion = new DAOAvion();
            List<Vol> listVol = new ArrayList<Vol>();
            String searchVolQuery = "SELECT v.ref, v.orig, v.dest, v.datedep, a.ref " +
                    "FROM VOL v, AVION a " +
                    "WHERE v.idAvion = a.id";
            preparedStatement = conn.prepareStatement(searchVolQuery);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Avion avion = null;
                for(Avion a : lesAvion) {
                    if (a.getRef().equals(rs.getString(5))) {
                        avion = a;
                        break;
                    }
                }
                Vol vol = new Vol(rs.getString(1), rs.getString(2), rs.getString(3), avion, rs.getDate(4));
                listVol.add(vol);
            }
            return listVol;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
