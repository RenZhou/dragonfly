package database;

import equipage.InvariantBroken;
import equipage.PNC;
import equipage.Pilote;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOPNC {

    private Connection conn;
    private PreparedStatement preparedStatement;

    public DAOPNC() {
        conn = ConnectionDB.getConnection();
    }

    public void addPNC(PNC pnc) throws InvariantBroken {
        try {
            DAOPersonne daoPersonne = new DAOPersonne();
            int idPNC = daoPersonne.addPersonne(pnc);
            String addPNCQuery = "INSERT INTO PNC(id) VALUES(?)";
            preparedStatement = conn.prepareStatement(addPNCQuery);
            preparedStatement.setInt(1, idPNC);
            preparedStatement.execute();
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public int searchPNC(PNC pnc) throws InvariantBroken {
        try {
            int idPNC = 0;
            String searchPNCQuery = "SELECT pnc.id FROM PNC pnc, PERSONNE p WHERE p.id = pnc.id and p.nom = ? and p.prenom = ?";
            preparedStatement = conn.prepareStatement(searchPNCQuery);
            preparedStatement.setString(1, pnc.getNom());
            preparedStatement.setString(2, pnc.getPrenom());
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                idPNC = rs.getInt(1);
            }
            return idPNC;
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public List<PNC> getAllPNC() throws InvariantBroken {
        try {
            List<PNC> listPNC = new ArrayList<PNC>();
            String searchPNCQuery = "SELECT pe.nom, pe.prenom FROM PNC p, PERSONNE pe where p.id = pe.id";
            preparedStatement = conn.prepareStatement(searchPNCQuery);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                PNC pnc = new PNC(rs.getString(1), rs.getString(2));
                listPNC.add(pnc);
            }
            return listPNC;
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public List<PNC> getAllPNC(int idEquipage) throws InvariantBroken {
        try {
            List<PNC> listPNC = new ArrayList<PNC>();
            String searchPNC = "SELECT p.nom, p.prenom " +
                    "FROM LISTPNC l, PNC pnc, PERSONNE p " +
                    "WHERE l.idEquipage = ? " +
                    "and l.idPNC = pnc.id " +
                    "and pnc.id = p.id ";
            preparedStatement = conn.prepareStatement(searchPNC);
            preparedStatement.setInt(1, idEquipage);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                PNC pnc = new PNC(rs.getString(1), rs.getString(2));
                listPNC.add(pnc);
            }
            return listPNC;
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

}
