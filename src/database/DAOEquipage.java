package database;

import aeroport.Vol;
import equipage.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DAOEquipage {
    private Connection conn;
    private PreparedStatement preparedStatement;
    private DAOCoPilote daoCoPilote;
    private DAOPilote daoPilote;
    private DAOPNC daopnc;
    private DAOVol daoVol;


    public DAOEquipage() {
        conn = ConnectionDB.getConnection();
        daoCoPilote = new DAOCoPilote();
        daoPilote = new DAOPilote();
        daopnc = new DAOPNC();
        daoVol = new DAOVol();
    }

    public void addEquipage(Equipage equipage) throws InvariantBroken {
        try {
            int idPilote = daoPilote.searchPilote(equipage.getPilote());
            int idCoPilote = daoCoPilote.searchCoPilote(equipage.getCoPilote());
            int idVol = daoVol.searchVol(equipage.getVol());

            int idEquipage = 0;
            String addPersonneQuery = "INSERT INTO EQUIPAGE(id, idVol, idPilote, idCopilote) VALUES(NULL, ?, ?, ?)";
            preparedStatement = conn.prepareStatement(addPersonneQuery, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, idVol);
            preparedStatement.setInt(2, idPilote);
            preparedStatement.setInt(2, idCoPilote);
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            while (rs.next()) {
                idEquipage = rs.getInt(1);
            }
            for (PNC pnc : equipage.getListPNC()) {
                int idPNC = daopnc.searchPNC(pnc);
                String addListPNC = "INSERT INTO LISTPNC(idEquipage, idPNC) VALUES (?, ?)";
                preparedStatement = conn.prepareStatement(addListPNC);
                preparedStatement.setInt(1, idEquipage);
                preparedStatement.setInt(2, idPNC);
                preparedStatement.execute();
            }
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public List<Equipage> getAllEquipage(List<Vol> lesVols, List<Pilote> lesPilotes, List<CoPilote> lesCoPilotes, List<PNC> lesPNC) {
        try {
            List<Equipage> listEquipageAvion = new ArrayList<Equipage>();
            String searchEquipageQuery = "" +
                    "SELECT e.id, v.ref, p1.nom, p1.prenom, p2.nom, p2.prenom " +
                    "FROM EQUIPAGE e, VOL v, PILOTE p, COPILOTE c, PERSONNE p1, PERSONNE p2 " +
                    "WHERE e.idVol = v.id " +
                    "and e.idPilote = p.id " +
                    "and e.idCopilote = c.id " +
                    "and p1.id = p.id " +
                    "and p2.id = c.id ";
            preparedStatement = conn.prepareStatement(searchEquipageQuery);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Vol vol = null;
                Pilote pilote = null;
                CoPilote coPilote = null;
                for(Vol v : lesVols) {
                    if (v.getRef().equals(rs.getString(2))) {
                        vol = v;
                        break;
                    }
                }
                for (Pilote p : lesPilotes) {
                    if (p.getNom().equals(rs.getString(3)) && p.getPrenom().equals(rs.getString(4))) {
                        pilote = p;
                        break;
                    }
                }
                for (CoPilote cp : lesCoPilotes) {
                    if (cp.getNom().equals(rs.getString(5)) && cp.getPrenom().equals(rs.getString(6))) {
                        coPilote = cp;
                        break;
                    }
                }
                Equipage equipage = new Equipage(vol);
                equipage.addPilote(pilote);
                equipage.addCoPilote(coPilote);
                equipage.addPNC(daopnc.getAllPNC(rs.getInt(1)));
                vol.addEquipage(equipage);
                listEquipageAvion.add(equipage);
            }
            return listEquipageAvion;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
