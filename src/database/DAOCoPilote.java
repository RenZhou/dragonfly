package database;

import equipage.CoPilote;
import equipage.InvariantBroken;
import equipage.PNC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DAOCoPilote {

    private Connection conn;
    private PreparedStatement preparedStatement;

    public DAOCoPilote() {
        conn = ConnectionDB.getConnection();
    }

    public void addCoPilote(CoPilote coPilote) throws InvariantBroken {
        try {
            DAOPersonne daoPersonne = new DAOPersonne();
            int idCoPilote = daoPersonne.addPersonne(coPilote);
            String addCoPiloteQuery = "INSERT INTO COPILOTE(id) VALUES(?)";
            preparedStatement = conn.prepareStatement(addCoPiloteQuery);
            preparedStatement.setInt(1, idCoPilote);
            preparedStatement.execute();
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public int searchCoPilote(CoPilote coPilote) throws InvariantBroken {
        try {
            int idCoPilote = 0;
            String searchCoPiloteQuery = "SELECT c.id FROM COPILOTE c, PERSONNE p WHERE p.id = c.id and p.nom = ? and p.prenom = ?";
            preparedStatement = conn.prepareStatement(searchCoPiloteQuery);
            preparedStatement.setString(1, coPilote.getNom());
            preparedStatement.setString(2, coPilote.getPrenom());
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                idCoPilote = rs.getInt(1);
            }
            return idCoPilote;
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public List<CoPilote> getAllCoPilote() throws InvariantBroken {
        try {
            List<CoPilote> listCoPIlote = new ArrayList<CoPilote>();
            String searchCoPIloteQuery = "SELECT pe.nom, pe.prenom FROM COPILOTE p, PERSONNE pe where p.id = pe.id";
            preparedStatement = conn.prepareStatement(searchCoPIloteQuery);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                CoPilote typeAvion = new CoPilote(rs.getString(1), rs.getString(2));
                listCoPIlote.add(typeAvion);
            }
            return listCoPIlote;
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }
}
