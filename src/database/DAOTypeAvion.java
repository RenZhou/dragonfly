package database;

import avion.TypeAvion;
import equipage.InvariantBroken;
import equipage.PNC;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ren on 10/01/2016.
 */
public class DAOTypeAvion {

    private Connection conn;
    private PreparedStatement preparedStatement;

    public DAOTypeAvion() {
        conn = ConnectionDB.getConnection();
    }

    public void addTypeAvion(TypeAvion typeAvion) throws InvariantBroken {
        try {
            String addPNCQuery = "INSERT INTO TYPEAVION(id, nom, minPNC, maxPNC) VALUES(NULL, ?, ?, ?)";
            preparedStatement = conn.prepareStatement(addPNCQuery);
            preparedStatement.setString(1, typeAvion.getNom());
            preparedStatement.setInt(2, typeAvion.getMinPNC());
            preparedStatement.setInt(3, typeAvion.getMaxPNC());
            preparedStatement.execute();
            System.out.println("[INFO] INSERTION DE DONNES : TYPEAVION " + typeAvion.getNom() + ", " + typeAvion.getMinPNC() + ", " + typeAvion.getMaxPNC());
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public int searchOneTypeAvion(String nom) throws InvariantBroken {
        try {
            int idTypeAvion = 0;
            String searchPNCQuery = "SELECT id FROM TYPEAVION WHERE nom = ?";
            preparedStatement = conn.prepareStatement(searchPNCQuery);
            preparedStatement.setString(1, nom);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                idTypeAvion = rs.getInt(1);
            }
            return idTypeAvion;
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public TypeAvion getTypeAvion(int id) throws InvariantBroken {
        try {
            TypeAvion typeAvion = null;
            String searchPNCQuery = "SELECT nom, minPNC, maxPNC FROM TYPEAVION WHERE id = ?";
            preparedStatement = conn.prepareStatement(searchPNCQuery);
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                typeAvion = new TypeAvion(rs.getString(1), rs.getInt(2), rs.getInt(3));
            }
            return typeAvion;
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public TypeAvion getTypeAvion(String nom) throws InvariantBroken {
        try {
            TypeAvion typeAvion = null;
            String searchPNCQuery = "SELECT nom, minPNC, maxPNC FROM TYPEAVION WHERE nom = ?";
            preparedStatement = conn.prepareStatement(searchPNCQuery);
            preparedStatement.setString(1, nom);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                typeAvion = new TypeAvion(rs.getString(1), rs.getInt(2), rs.getInt(3));
            }
            return typeAvion;
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public List<TypeAvion> getAllTypeAvion() throws InvariantBroken {
        try {
            List<TypeAvion> listTypeAvion = new ArrayList<TypeAvion>();
            String searchTypeAvionQuery = "SELECT nom, minPNC, maxPNC FROM TYPEAVION";
            preparedStatement = conn.prepareStatement(searchTypeAvionQuery);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                TypeAvion typeAvion = new TypeAvion(rs.getString(1), rs.getInt(2), rs.getInt(3));
                listTypeAvion.add(typeAvion);
            }
            return listTypeAvion;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
