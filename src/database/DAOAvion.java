package database;

import avion.Avion;
import avion.TypeAvion;
import equipage.InvariantBroken;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOAvion {

    private Connection conn;
    private PreparedStatement preparedStatement;

    public DAOAvion() {
        conn = ConnectionDB.getConnection();
    }

    public void addAvion(Avion avion) throws InvariantBroken {
        try {
            DAOTypeAvion daoTypeAvion = new DAOTypeAvion();
            int idTypeAvion = daoTypeAvion.searchOneTypeAvion(avion.getTypeAvion().getNom());
            String addAvionQuery = "INSERT INTO AVION(id, idTypeavion, ref) VALUES(NULL, ?, ?)";
            preparedStatement = conn.prepareStatement(addAvionQuery);
            preparedStatement.setInt(1, idTypeAvion);
            preparedStatement.setString(2, avion.getRef());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new InvariantBroken();
        }
    }

    public List<Avion> getAllAvion(List<TypeAvion> listTypeAvion) throws InvariantBroken {
        try {
            List<Avion> listAvion = new ArrayList<Avion>();
            String getAvionQuery = "SELECT a.ref, t.nom FROM AVION a, TYPEAVION t WHERE a.idTypeavion = t.id";
            preparedStatement = conn.prepareStatement(getAvionQuery);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                String data = rs.getString(2);
                for (TypeAvion t : listTypeAvion) {
                    if (t.getNom().equals(rs.getString(2))) {
                        Avion avion = new Avion(t, rs.getString(1));
                        listAvion.add(avion);
                        break;
                    }
                }
            }
            return listAvion;
        } catch (SQLException e) {
            throw new InvariantBroken();
        }
    }
}
