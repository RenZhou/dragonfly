package database;

import java.sql.*;

public class ConnectionDB {

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_NAME = "dragonfly";
    private static final String DB_URL = "jdbc:mysql://192.168.99.100:3306/";

    //  Database credentials
    //mysql -uadmin -pOjC8BBB1rk0n
    private static final String USER = "admin";
    private static final String PASS = "EU66FQpWuZLC";

    private static ConnectionDB connectionDB;

    private static Connection conn;

    private ConnectionDB() {
        conn = null;
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            conn.setCatalog(DB_NAME);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static Connection getConnection() {
        if (connectionDB == null) {
            connectionDB = new ConnectionDB();
        }
        return getConn();
    }

    private static Connection getConn() {
        return conn;
    }

    public static void removeDatabase() {
        Connection conn = getConnection();
        Statement statement = null;
        try {
            statement = conn.createStatement();
            statement.execute("DROP DATABASE IF EXISTS " + DB_NAME);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void createDatabase() {
        Connection conn = getConnection();
        Statement statement = null;
        try {
            statement = conn.createStatement();
            statement.execute("CREATE DATABASE IF NOT EXISTS " + DB_NAME + " DEFAULT CHARACTER SET utf8 " +
                    "  DEFAULT COLLATE utf8_general_ci");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void resetDatabase() {
        removeDatabase();
        createDatabase();
    }

    public static void loadDatabase() {
        Connection conn = getConnection();
        Statement statement = null;
        try {
            conn.setCatalog(DB_NAME);
            statement = conn.createStatement();
            statement.execute("INSERT INTO TYPEAVION VALUES (null, 'BOEING', 2, 4)");
            statement.execute("INSERT INTO TYPEAVION VALUES (null, 'FLIGHT', 2, 5)");
            statement.execute("INSERT INTO AVION VALUES (null, 1, 'B755')");
            statement.execute("INSERT INTO AVION VALUES (null, 2, 'B756')");
            statement.execute("INSERT INTO AVION VALUES (null, 1, 'F755')");
            statement.execute("INSERT INTO PERSONNE VALUES (null, 'zhou', 'ren')");
            statement.execute("INSERT INTO PERSONNE VALUES (null, 'riviere', 'aurelien')");
            statement.execute("INSERT INTO PERSONNE VALUES (null, 'collas', 'robin')");
            statement.execute("INSERT INTO PERSONNE VALUES (null, 'avril', 'caitlyn')");
            statement.execute("INSERT INTO PERSONNE VALUES (null, 'miss', 'fortune')");
            statement.execute("INSERT INTO PILOTE VALUES (1)");
            statement.execute("INSERT INTO PILOTE VALUES (2)");
            statement.execute("INSERT INTO COPILOTE VALUES (3)");
            statement.execute("INSERT INTO PNC VALUES (4)");
            statement.execute("INSERT INTO PNC VALUES (5)");
            statement.execute("INSERT INTO AVIONHANDLE VALUES (1, 1)");
            statement.execute("INSERT INTO AVIONHANDLE VALUES (1, 2)");
            statement.execute("INSERT INTO AVIONHANDLE VALUES (2, 2)");
            statement.execute("INSERT INTO VOL VALUES(null, 'AF347', 'FRPRS', 'FRLYN', now(), 1, 1)");
            statement.execute("INSERT INTO VOL VALUES(null, 'AF348', 'FRLYN', 'FRPRS', now(), 2, 1)");
            statement.execute("INSERT INTO VOL VALUES(null, 'AF349', 'FRLYN', 'FRPRS', now(), 3, 2)");
            statement.execute("INSERT INTO EQUIPAGE VALUES (null, 1, 1, 3)");
            statement.execute("INSERT INTO EQUIPAGE VALUES (null, 1, 2, 3)");
            statement.execute("INSERT INTO LISTPNC VALUES (1, 4)");
            statement.execute("INSERT INTO LISTPNC VALUES (1, 5)");
            statement.execute("INSERT INTO LISTPNC VALUES (2, 4)");
            statement.execute("INSERT INTO LISTPNC VALUES (2, 5)");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void initDatabase() {
        Connection conn = getConnection();
        Statement statement = null;
        try {
            conn.setCatalog(DB_NAME);
            statement = conn.createStatement();
            String volQuery =
                    "CREATE TABLE IF NOT EXISTS VOL(" +
                            "    id INTEGER NOT NULL AUTO_INCREMENT," +
                            "    ref VARCHAR(5)," +
                            "    orig VARCHAR(5)," +
                            "    dest VARCHAR(5)," +
                            "    datedep DATE," +
                            "    idAvion INTEGER," +
                            "    idEquipage INTEGER," +
                            "    PRIMARY KEY (id)" +
                            ")";
            String avionQuery =
                    "CREATE TABLE IF NOT EXISTS AVION(" +
                            "id INTEGER NOT NULL AUTO_INCREMENT," +
                            "idTypeavion INTEGER," +
                            "ref VARCHAR(6)," +
                            "PRIMARY KEY (id)" +
                            ")";
            String equipageQuery =
                    "CREATE TABLE IF NOT EXISTS EQUIPAGE(" +
                            "id INTEGER NOT NULL AUTO_INCREMENT," +
                            "idVol INTEGER," +
                            "idPilote INTEGER," +
                            "idCopilote INTEGER," +
                            "PRIMARY KEY (id)" +
                            ")";
            String listPNCQuery =
                    "CREATE TABLE IF NOT EXISTS LISTPNC(" +
                            "idEquipage INTEGER," +
                            "idPNC INTEGER," +
                            "PRIMARY KEY (idEquipage, idPNC)" +
                            ")";
            String personneQuery =
                    "CREATE TABLE IF NOT EXISTS PERSONNE(" +
                            "id INTEGER NOT NULL AUTO_INCREMENT," +
                            "nom VARCHAR(20)," +
                            "prenom VARCHAR(20)," +
                            "UNIQUE(nom, prenom)," +
                            "PRIMARY KEY (id)" +
                            ")";
            String piloteQuery =
                    "CREATE TABLE IF NOT EXISTS PILOTE(" +
                            "id INTEGER," +
                            "PRIMARY KEY (id)" +
                            ")";
            String copiloteQuery =
                    "CREATE TABLE IF NOT EXISTS COPILOTE(" +
                            "id INTEGER," +
                            "PRIMARY KEY (id)" +
                            ")";
            String pncQuery =
                    "CREATE TABLE IF NOT EXISTS PNC(" +
                            "id INTEGER," +
                            "PRIMARY KEY (id)" +
                            ")";
            String AvionHandleQuery =
                    "CREATE TABLE IF NOT EXISTS AVIONHANDLE(" +
                            "idPersonne INTEGER," +
                            "idTypeAvion INTEGER," +
                            "PRIMARY KEY (idPersonne, idTypeAvion)" +
                            ")";
            String typeavionQuery =
                    "CREATE TABLE IF NOT EXISTS TYPEAVION(" +
                            "id INTEGER NOT NULL AUTO_INCREMENT," +
                            "nom VARCHAR(16)," +
                            "minPNC INTEGER," +
                            "maxPNC INTEGER," +
                            "PRIMARY KEY (id)," +
                            "UNIQUE(nom)" +
                            ")";
            statement.execute(volQuery);
            statement.execute(equipageQuery);
            statement.execute(avionQuery);
            statement.execute(typeavionQuery);
            statement.execute(personneQuery);
            statement.execute(piloteQuery);
            statement.execute(copiloteQuery);
            statement.execute(pncQuery);
            statement.execute(AvionHandleQuery);
            statement.execute(listPNCQuery);
            statement.execute("ALTER TABLE AVION        ADD FOREIGN KEY (idTypeavion)   REFERENCES TYPEAVION(id);");
            statement.execute("ALTER TABLE VOL          ADD FOREIGN KEY (idAvion)       REFERENCES AVION(id)        ON DELETE SET NULL;");
            statement.execute("ALTER TABLE EQUIPAGE     ADD FOREIGN KEY (idPilote)      REFERENCES PILOTE(id)       ON DELETE CASCADE;");
            statement.execute("ALTER TABLE EQUIPAGE     ADD FOREIGN KEY (idCopilote)    REFERENCES COPILOTE(id)     ON DELETE CASCADE;");
            statement.execute("ALTER TABLE EQUIPAGE     ADD FOREIGN KEY (idVol)         REFERENCES VOL(id)          ON DELETE CASCADE;");
            statement.execute("ALTER TABLE LISTPNC      ADD FOREIGN KEY (idEquipage)    REFERENCES EQUIPAGE(id);");
            statement.execute("ALTER TABLE LISTPNC      ADD FOREIGN KEY (idPNC)         REFERENCES PNC(id)          ON DELETE CASCADE;");
            statement.execute("ALTER TABLE PILOTE       ADD FOREIGN KEY (id)            REFERENCES PERSONNE(id)     ON DELETE CASCADE;");
            statement.execute("ALTER TABLE COPILOTE     ADD FOREIGN KEY (id)            REFERENCES PERSONNE(id)     ON DELETE CASCADE;");
            statement.execute("ALTER TABLE PNC          ADD FOREIGN KEY (id)            REFERENCES PERSONNE(id)     ON DELETE CASCADE;");
            statement.execute("ALTER TABLE AVIONHANDLE  ADD FOREIGN KEY (idPersonne)      REFERENCES PERSONNE(id);");
            statement.execute("ALTER TABLE AVIONHANDLE  ADD FOREIGN KEY (idTypeAvion)   REFERENCES TYPEAVION(id);");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
