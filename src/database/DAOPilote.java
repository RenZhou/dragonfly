package database;

import equipage.InvariantBroken;
import equipage.Pilote;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DAOPilote {
    private Connection conn;
    private PreparedStatement preparedStatement;

    public DAOPilote() {
        conn = ConnectionDB.getConnection();
    }

    public void addPilote(Pilote pilote) throws InvariantBroken {
        try {
            DAOPersonne daoPersonne = new DAOPersonne();
            int idPilote = daoPersonne.addPersonne(pilote);
            String addPiloteQuery = "INSERT INTO PILOTE(id) VALUES(?)";
            preparedStatement = conn.prepareStatement(addPiloteQuery);
            preparedStatement.setInt(1, idPilote);
            preparedStatement.execute();
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public int searchPilote(Pilote pilote) throws InvariantBroken {
        try {
            int idPilote = 0;
            String searchPiloteQuery = "SELECT p.id FROM PILOTE p, PERSONNE pe WHERE pe.id = p.id and pe.nom = ? and pe.prenom = ?";
            preparedStatement = conn.prepareStatement(searchPiloteQuery);
            preparedStatement.setString(1, pilote.getNom());
            preparedStatement.setString(2, pilote.getPrenom());
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                idPilote = rs.getInt(1);
            }
            return idPilote;
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public void removePilote(Pilote pilote) throws InvariantBroken {
        try {
            String searchPilote =
                    "SELECT * FROM PILOTE P, PERSONNE PE WHERE P.id = PE.id AND PE.nom = ? AND PE.prenom = ?";
            preparedStatement = conn.prepareStatement(searchPilote);
            preparedStatement.setString(1, pilote.getNom());
            preparedStatement.setString(2, pilote.getPrenom());
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                DAOPersonne daoPersonne = new DAOPersonne();
                daoPersonne.removePersonne(pilote);
            }
        } catch (Exception e) {
            throw new InvariantBroken();
        }
    }

    public List<Pilote> getAllPilote() throws InvariantBroken {
        try {
            List<Pilote> listPilote = new ArrayList<Pilote>();
            String searchPiloteQuery = "SELECT pe.nom, pe.prenom FROM PILOTE p, PERSONNE pe where p.id = pe.id";
            preparedStatement = conn.prepareStatement(searchPiloteQuery);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Pilote typeAvion = new Pilote(rs.getString(1), rs.getString(2));
                listPilote.add(typeAvion);
            }
            return listPilote;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
