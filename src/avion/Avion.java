package avion;

public class Avion {

    private TypeAvion typeAvion;
    private String ref;

    public Avion(TypeAvion typeAvion, String ref) {
        this.typeAvion = typeAvion;
        this.ref = ref;
    }

    public String getRef() {
        return ref;
    }

    public TypeAvion getTypeAvion() {
        return typeAvion;
    }

    @Override
    public String toString() {
        return ref;
    }

}
