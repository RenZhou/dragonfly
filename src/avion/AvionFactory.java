package avion;

public class AvionFactory {

    public Avion create(String typeAvion, String ref) throws AvionCreationException {
        if(typeAvion.compareTo("B747") == 0) {
            TypeAvion ta = new TypeAvion(typeAvion, 3, 4);
            return new Avion(ta, ref);
        }
        if(typeAvion.compareTo("A320") == 0) {
            TypeAvion ta = new TypeAvion(typeAvion, 2, 3);
            return new Avion(ta, ref);
        }
        throw new AvionCreationException();
    }

    private class AvionCreationException extends Exception {
    }
}
