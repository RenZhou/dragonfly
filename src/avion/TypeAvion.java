package avion;

import equipage.EquipageException;
import equipage.InvariantBroken;
import equipage.Personne;

import java.util.ArrayList;
import java.util.List;

public class TypeAvion {
    private String nom;
    private List<Personne> listPersonne;
    private int minPNC;
    private int maxPNC;

    public TypeAvion(String nom) {
        this.nom = nom;
        this.minPNC = 2;
        this.maxPNC = 4;
        listPersonne = new ArrayList<Personne>();
    }

    public TypeAvion(String nom, int minPNC, int maxPNC) {
        this(nom);
        this.minPNC = minPNC;
        this.maxPNC = maxPNC;
    }

    public String getNom() {
        return nom;
    }

    public int getMinPNC() {
        return minPNC;
    }

    public int getMaxPNC() {
        return maxPNC;
    }

    public void setMinPNC(int minPNC) {
        this.minPNC = minPNC;
    }

    public void setMaxPNC(int maxPNC) {
        this.maxPNC = maxPNC;
    }

    public void addQualifie(Personne personne) throws EquipageException {
        for (Personne p : listPersonne) {
            if (personne == p) {
                throw new EquipageException();
            }
        }
        listPersonne.add(personne);
    }

    public void delQualifie(Personne personne) throws EquipageException {
        for (Personne p : listPersonne) {
            if (p == personne) {
                listPersonne.remove(p);
            }
        }
        throw new EquipageException();
    }

    public void purgeQualifies() throws EquipageException, InvariantBroken {
        for (Personne p : listPersonne) {
            p.delQualification(this, true);
        }
        listPersonne.clear();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof TypeAvion)) return false;
        TypeAvion otherMyClass = (TypeAvion) other;
        return (this.nom.equals(otherMyClass.nom));
    }

    @Override
    public String toString() {
        return nom;
    }
}
